var Utils         = require("../../utils")
  , AbstractQuery = require('../abstract/query')
  , Request = require('tedious').Request
  , moment      = require("moment")

module.exports = (function() {
  var Query = function(client, sequelize, callee, options) {
    this.client    = client
    this.callee    = callee
    this.sequelize = sequelize
    this.options   = Utils._.extend({
      logging: console.log,
      plain: false,
      raw: false
    }, options || {})
    this.rows = []

    this.checkLoggingOption()
  }

  Utils.inherit(Query, AbstractQuery)
  Query.prototype.run = function(sql) {
    var self = this
    this.sql = sql

    //console.log(sql);
    
    if (this.options.logging !== false) {
      this.options.logging('Executing: ' + this.sql)
    }

    var dates = this.sql.match(/\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d(\.\d\d\d)?( ?[\+-]\d\d(:\d\d)?)?/g) || [];

    dates.forEach(function(date) {
      var momentDate = moment(date, 'YYYY-MM-DD HH:mm:ss.SSSZ').format();
      self.sql = self.sql.replace(date, momentDate);
    });

    this.sql = self.sql;

    var request = new Request(this.sql, function(err, rowCount) {
      if(err){
        console.log(err)
        self.emit('error', new Error(err), self.callee)
      }
      else{
        var results = self.options.raw ? self.rows : getResults.call(self)
        self.emit('success', results)
      }
    })

    request.on('row', function(columns){
      var row = {}
      Utils._.forEach(columns, function(col){
        row[col.metadata.colName] = col.value
      })
      self.rows.push(row)
    })

    this.client.execSql(request);
    return this
  }

  var getMomentDates = function(thisRows) {
    var rows = thisRows;
    for(var i = 0; i < rows.length; i++) {
      var row = rows[i];
      Object.keys(row).forEach(function(field) {
        if(field === 'created_at' || field === 'updated_at' || field === 'deleted_at') return;
        if(row[field]) {
          var dates = row[field].toString().match(/\d\d\d\d-\d\d-\d\d.*/g);
          if(dates) {
            var date = dates[0];
            rows[i][field] = moment(date).format();
          }
        }
      });
    }

    return rows;
  }

  var getResults = function(){
    var results = this.callee
      , isSelectTableName = (this.sql.indexOf("SELECT TABLE_NAME") === 0)

    var rows = null, row = null;
    if(isSelectTableName){
      rows = getMomentDates(this.rows);
      results = rows.map(function(row) { return Utils._.values(row) })
    }
    else if(this.send('isSelectQuery')){
      rows = getMomentDates(this.rows);
      results = this.send('handleSelectQuery', rows)
    }
    else if(this.send('isInsertQuery')){
      row = getMomentDates(this.rows)[0];
      this.send('handleInsertQuery', row)
    }

    return results
  }

  return Query
})()


