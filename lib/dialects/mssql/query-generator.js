var Utils       = require("../../utils")
  , util        = require("util")
  , DataTypes   = require("../../data-types")
  , tables      = {}
  , primaryKeys = {}

var processAndEscapeValue = function(value) {
    var processedValue = value
    if (typeof value === 'boolean') {
        processedValue = value ? 1 : 0
    }

    var escapedValue =  require("../abstract/query-generator").escape(processedValue)
    escapedValue = escapedValue.replace(/\\'/g, "''")
    return escapedValue
}

module.exports = (function() {
  var QueryGenerator = {
    options: {},
    dialect: 'mssql',

    addSchema: function(opts) {
      var tableName         = undefined
      var schema            = (!!opts.options && !!opts.options.schema ? opts.options.schema : undefined)
      var schemaDelimiter   = (!!opts.options && !!opts.options.schemaDelimiter ? opts.options.schemaDelimiter : undefined)

      if (!!opts.tableName) {
        tableName = opts.tableName
      }
      else if (typeof opts === "string") {
        tableName = opts
      }

      if (!schema || schema.toString().trim() === "") {
        return tableName
      }

      return QueryGenerator.addQuotes(schema) + '.' + QueryGenerator.addQuotes(tableName)
    },

    createSchema: function(schema) {
      var query = "CREATE SCHEMA <%= schema%>;"
      return Utils._.template(query)({schema: schema})
    },

    dropSchema: function(schema) {
      var query = "DROP SCHEMA <%= schema%>;"
      return Utils._.template(query)({schema: schema})
    },

    showSchemasQuery: function() {
      return "SELECT schema_name FROM information_schema.schemata WHERE schema_name <> 'INFORMATION_SCHEMA' AND schema_name != 'sys' AND schema_name LIKE 'db[_]%';"
    },

    createTableQuery: function(tableName, attributes, options) {
      var query   = "IF OBJECT_ID('<%= unquotedTable %>', N'U') IS NULL CREATE TABLE <%= table %> (<%= attributes%>)"
        , attrStr = []
        ,primaryKeys = Utils._.keys(Utils._.pick(attributes, function(dataType){
          return dataType.indexOf('PRIMARY KEY') >= 0
        }));

      for (var attr in attributes) {
        if (attributes.hasOwnProperty(attr)) {
          var dataType = attributes[attr]
          if(primaryKeys.length > 1){
            dataType = dataType.replace(/ PRIMARY KEY/, '')
          }
          attrStr.push(QueryGenerator.addQuotes(attr) + " " + dataType)
        }
      }

      if (primaryKeys.length > 1) {
        attrStr.push('PRIMARY KEY(' + primaryKeys.map(function(column){return QueryGenerator.addQuotes(column)}).join(', ') + ')')
      }

      var values = {
          unquotedTable: tableName,
          table: QueryGenerator.addQuotes(tableName),
          attributes: attrStr.join(", ")
        }

      return Utils._.template(query)(values).trim() + ";"
    },

    /*
     Returns a query for dropping a table.
     */
    dropTableQuery: function(tableName, options) {
      var query = "IF  OBJECT_ID('<%= unquotedTable %>') IS NOT NULL DROP TABLE <%= table %>;"

      return Utils._.template(query)({
        unquotedTable: tableName,
        table: QueryGenerator.addQuotes(tableName)
      })
    },

    /*
     Returns a rename table query.
     Parameters:
     - originalTableName: Name of the table before execution.
     - futureTableName: Name of the table after execution.
     */
    renameTableQuery: function(originalTableName, futureTableName) {
      throwMethodUndefined('renameTableQuery');
    },

    /*
     Returns a query, which gets all available table names in the database.
     */
    showTablesQuery: function() {
      return "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE'";
    },

    /*
     Returns a query, which adds an attribute to an existing table.
     Parameters:
     - tableName: Name of the existing table.
     - attributes: A hash with attribute-attributeOptions-pairs.
     - key: attributeName
     - value: A hash with attribute specific options:
     - type: DataType
     - defaultValue: A String with the default value
     - allowNull: Boolean
     */
    addColumnQuery: function(tableName, attributes) {
      throwMethodUndefined('addColumnQuery');
    },

    /*
     Returns a query, which removes an attribute from an existing table.
     Parameters:
     - tableName: Name of the existing table
     - attributeName: Name of the obsolete attribute.
     */
    removeColumnQuery: function(tableName, attributeName) {
      throwMethodUndefined('removeColumnQuery');
    },

    /*
     Returns a query, which modifies an existing attribute from a table.
     Parameters:
     - tableName: Name of the existing table.
     - attributes: A hash with attribute-attributeOptions-pairs.
     - key: attributeName
     - value: A hash with attribute specific options:
     - type: DataType
     - defaultValue: A String with the default value
     - allowNull: Boolean
     */
    changeColumnQuery: function(tableName, attributes) {
      throwMethodUndefined('changeColumnQuery');
    },

    /*
     Returns a query, which renames an existing attribute.
     Parameters:
     - tableName: Name of an existing table.
     - attrNameBefore: The name of the attribute, which shall be renamed.
     - attrNameAfter: The name of the attribute, after renaming.
     */
    renameColumnQuery: function(tableName, attrNameBefore, attrNameAfter) {
      throwMethodUndefined('renameColumnQuery');
    },

    selectQuery: function(tableName, options) {
        var abstractSelectQuery = require("../abstract/query-generator").selectQuery.bind(this);
        var query = abstractSelectQuery(tableName, options);
        // SQL Server always uses case-insensitive "LIKE"
        return query.replace('ILIKE', 'LIKE');
    },

    /*
     Returns an insert into command for multiple values.
     Parameters: table name + list of hashes of attribute-value-pairs.
     */
    bulkInsertQuery: function(tableName, attrValueHashes) {
      var tuples = []

      Utils._.forEach(attrValueHashes, function(attrValueHash) {
        tuples.push("(" +
          Utils._.values(attrValueHash).map(processAndEscapeValue).join(",") +
          ")")
      })

      var table      = QueryGenerator.addQuotes(tableName)
      var attributes = Object.keys(attrValueHashes[0]).map(function(attr){return QueryGenerator.addQuotes(attr)}).join(",")

      var query  = "INSERT INTO " + table + " (" + attributes + ") VALUES " + tuples.join(",") + ";"

      return query
    },
    /*
     Returns an update query.
     Parameters:
     - tableName -> Name of the table
     - values -> A hash with attribute-value-pairs
     - where -> A hash with conditions (e.g. {name: 'foo'})
     OR an ID as integer
     OR a string with conditions (e.g. 'name="foo"').
     If you use a string, you have to escape it on your own.
     */
    updateQuery: function(tableName, attrValueHash, where) {
      attrValueHash.id = null;
      attrValueHash = Utils.removeNullValuesFromHash(attrValueHash, this.options.omitNull)

      var values = []


      for (var key in attrValueHash) {
        var value  = attrValueHash[key]
          , _value = processAndEscapeValue(value)

        values.push(this.addQuotes(key) + "=" + _value)
      }

      var query = "UPDATE " + this.addQuotes(tableName) +
        " SET " + values.join(",") +
        " WHERE " + this.getWhereConditions(where)

      return query
    },

    /*
     Returns a deletion query.
     Parameters:
     - tableName -> Name of the table
     - where -> A hash with conditions (e.g. {name: 'foo'})
     OR an ID as integer
     OR a string with conditions (e.g. 'name="foo"').
     If you use a string, you have to escape it on your own.
     Options:
     - limit -> Maximaum count of lines to delete
     */
    deleteQuery: function(tableName, where, options) {
      options = options || {}

      var table = QueryGenerator.addQuotes(tableName)
      where = QueryGenerator.getWhereConditions(where)

      if(Utils._.isUndefined(options.limit)) {
        options.limit = 1;
      }

      var query;

      if(options.limit){
        query = "DELETE TOP(<%= limit %>) FROM " + table + " WHERE " + where
      }
      else{
        query = "DELETE FROM " + table + " WHERE " + where
      }

      return Utils._.template(query)(options);
    },

    /*
     Returns a bulk deletion query.
     Parameters:
     - tableName -> Name of the table
     - where -> A hash with conditions (e.g. {name: 'foo'})
     OR an ID as integer
     OR a string with conditions (e.g. 'name="foo"').
     If you use a string, you have to escape it on your own.
     */
    bulkDeleteQuery: function(tableName, where, options) {
      options = options || {}

      var table = QueryGenerator.addQuotes(tableName)
      where = QueryGenerator.getWhereConditions(where)

      var query = "DELETE FROM " + table + " WHERE " + where

      return query
    },

    /*
     Returns an update query.
     Parameters:
     - tableName -> Name of the table
     - values -> A hash with attribute-value-pairs
     - where -> A hash with conditions (e.g. {name: 'foo'})
     OR an ID as integer
     OR a string with conditions (e.g. 'name="foo"').
     If you use a string, you have to escape it on your own.
     */
    incrementQuery: function(tableName, attrValueHash, where) {
      attrValueHash = Utils.removeNullValuesFromHash(attrValueHash, this.options.omitNull)

      var values = []

      for (var key in attrValueHash) {
        if(attrValueHash.hasOwnProperty(key)){
          var value  = attrValueHash[key]
            , _value = processAndEscapeValue(value)

          values.push(QueryGenerator.addQuotes(key) + "=" + QueryGenerator.addQuotes(key) + " + " + _value)
        }
      }

      var table = QueryGenerator.addQuotes(tableName)
      values = values.join(",")
      where = QueryGenerator.getWhereConditions(where)

      var query = "UPDATE " + table + " SET " + values + " WHERE " + where

      return query
    },

    /*
     Returns an add index query.
     Parameters:
     - tableName -> Name of an existing table.
     - attributes:
     An array of attributes as string or as hash.
     If the attribute is a hash, it must have the following content:
     - attribute: The name of the attribute/column
     - length: An integer. Optional
     - order: 'ASC' or 'DESC'. Optional
     - options:
     - indicesType: UNIQUE|FULLTEXT|SPATIAL
     - indexName: The name of the index. Default is <tableName>_<attrName1>_<attrName2>
     - parser
     */
    addIndexQuery: function(tableName, attributes, options) {
      throwMethodUndefined('addIndexQuery');
    },

    /*
     Returns an show index query.
     Parameters:
     - tableName: Name of an existing table.
     - options:
     - database: Name of the database.
     */
    showIndexQuery: function(tableName, options) {
      throwMethodUndefined('showIndexQuery');
    },

    /*
     Returns a remove index query.
     Parameters:
     - tableName: Name of an existing table.
     - indexNameOrAttributes: The name of the index as string or an array of attribute names.
     */
    removeIndexQuery: function(tableName, indexNameOrAttributes) {
      throwMethodUndefined('removeIndexQuery');
    },

    /*
     Takes something and transforms it into values of a where condition.
     */
    getWhereConditions: function(smth, tableName) {
      var result = null

      if (Utils.isHash(smth)) {
        smth = Utils.prependTableNameToHash(tableName, smth)
        result = this.hashToWhereConditions(smth)
      }
      else if (typeof smth === "number") {
        smth = Utils.prependTableNameToHash(tableName, { id: smth })
        result = this.hashToWhereConditions(smth)
      }
      else if (typeof smth === "string") {
        result = smth
      }
      else if (Array.isArray(smth)) {
        result = Utils.format(smth)
      }

      return result
    },

    /*
     Takes a hash and transforms it into a mysql where condition: {key: value, key2: value2} ==> key=value AND key2=value2
     The values are transformed by the relevant datatype.
     */
//     hashToWhereConditions: function(hash) {
//       var result = []
//
//       for (var key in hash) {
//         var value = hash[key]
//
//         //handle qualified key names
//         var _key   = key.split('.').map(function(col){return QueryGenerator.addQuotes(col)}).join(".")
//           , _value = null
//
//         if (Array.isArray(value)) {
//           if (value.length == 0) { value = [null] }
//           _value = "(" + value.map(function(subValue) {
//             return QueryGenerator.pgEscape(subValue);
//           }).join(',') + ")"
//
//           result.push([_key, _value].join(" IN "))
//         }
//         else if ((value) && (typeof value === "object")) {
//           //using as sentinel for join column => value
//           _value = value['eq'].toString().split('.').map(function(col){return QueryGenerator.addQuotes(col, "'")}).join(".")
//           result.push([_key, _value].join("="))
//         } else {
//           _value = QueryGenerator.pgEscape(value)
//           result.push((_value == 'NULL') ? _key + " IS NULL" : [_key, _value].join("="))
//         }
//       }
//
//       return result.join(' AND ')
//     },

    /*
     This method transforms an array of attribute hashes into equivalent
     sql attribute definition.
     */
    attributesToSQL: function(attributes) {
      var result = {}

      for (var name in attributes) {
        var dataType = attributes[name]

        if (Utils.isHash(dataType)) {
          var template;

          if (dataType.type.toString() === DataTypes.ENUM.toString()) {
            template = "VARCHAR(30) CHECK (" + name + " IN (" + Utils._.map(dataType.values, function(value) {
                return QueryGenerator.addQuotes(this.escape(value), "'")
              }).join(", ") + ")" + ")";
          } else {
            template = dataType.type.toString();
          }

          if(dataType.type.toString().indexOf('TINYINT(') >= 0){
            template = template.replace(/TINYINT\(.\)/, 'TINYINT')
          }


          if(dataType.hasOwnProperty('zeroFill') && dataType.zeroFill){
            throw new Error('MSSQL does not support ZEROFILL')
          }

          if(dataType.hasOwnProperty('unsigned') && dataType.unsigned){
            throw new Error('MSSQL does not support UNSIGNED')
          }

          if(dataType.type.toString() === "DATETIME") {
            template = "DATETIMEOFFSET"
          }

          if (dataType.hasOwnProperty('allowNull') && (!dataType.allowNull)) {
            template += " NOT NULL"
          }

          if (dataType.autoIncrement) {
            template += " IDENTITY"
          }

          if ((dataType.defaultValue !== undefined) && (dataType.defaultValue != DataTypes.NOW)) {
            if(dataType.type.toString().indexOf('TINYINT(') >= 0) {
              if(this.escape(dataType.defaultValue) === 'false') {
                dataType.defaultValue = 0;
              }
              else if(this.escape(dataType.defaultValue) === 'true') {
                dataType.defaultValue = 1;
              }
            }

            template += " DEFAULT " + this.escape(dataType.defaultValue);
          }

          if (dataType.unique) {
            template += " UNIQUE"
          }


          if (dataType.primaryKey) {
            template += " PRIMARY KEY"
          }

          if(dataType.allowNull || (!(dataType.autoIncrement || dataType.defaultValue || dataType.unique || dataType.primaryKey) && dataType.allowNull === undefined)){
            template += " NULL"
          }

          if(dataType.references) {
            template += " REFERENCES " + QueryGenerator.addQuotes(dataType.references)


            if(dataType.referencesKey) {
              template += " (" + QueryGenerator.addQuotes(dataType.referencesKey) + ")"
            } else {
              template += " (" + QueryGenerator.addQuotes('id') + ")"
            }

            if(dataType.onDelete) {
              template += " ON DELETE " + dataType.onDelete.toUpperCase()
            }

            if(dataType.onUpdate) {
              template += " ON UPDATE " + dataType.onUpdate.toUpperCase()
            }

          }

          result[name] = template
        } else {
          result[name] = dataType
        }
      }

      return result
    },

    /*
     Returns all auto increment fields of a factory.
     */
    findAutoIncrementField: function(factory) {
      var fields = []

      for (var name in factory.attributes) {
        if (factory.attributes.hasOwnProperty(name)) {
          var definition = factory.attributes[name]

          if (definition && (definition.indexOf('IDENTITY') > -1)) {
            fields.push(name)
          }
        }
      }

      return fields
    },

    enableForeignKeyConstraintsQuery: function() {
      return "exec sp_msforeachtable @command1=\"print '?'\", @command2=\"ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all\""
    },

    /*
     Globally disable foreign key constraints
     */
    disableForeignKeyConstraintsQuery: function() {
      return "EXEC sp_msforeachtable \"ALTER TABLE ? NOCHECK CONSTRAINT all\""
    },

    removeQuotes: function (s, quoteChar) {
      quoteChar = quoteChar || '"';
      return s.replace(new RegExp(quoteChar, 'g'), '');
    },

    addQuotes: function (s, quoteChar) {
      quoteChar = quoteChar || '"';
      return QueryGenerator.removeQuotes(s, quoteChar)
        .split('.')
        .map(function(e) { return quoteChar + String(e) + quoteChar })
        .join('.');
    },

    pgEscape: function (val) {
      if (val === undefined || val === null) {
        return 'NULL';
      }

      switch (typeof val) {
        case 'boolean': return (val) ? 'true' : 'false';
        case 'number': return val+'';
        case 'object':
          if (Array.isArray(val)) {
            return 'ARRAY['+ val.map(function(it) { return QueryGenerator.pgEscape(it) }).join(',') +']';
          }
      }

      if (val instanceof Date) {
        val = QueryGenerator.pgSqlDate(val);
      }

      // http://www.postgresql.org/docs/8.2/static/sql-syntax-lexical.html#SQL-SYNTAX-STRINGS
      val = val.replace(/'/g, "''");
      return "'"+val+"'";
    },

    quoteIdentifier: function(identifier, force) {
        if (identifier === '*') return identifier
        return Utils.addTicks(identifier, '"')
    },

    quoteIdentifiers: function(identifiers, force) {
        return identifiers.split('.').map(function(v) { return this.quoteIdentifier(v, force) }.bind(this)).join('.')
    },

    quoteTable: function(table) {
        return this.quoteIdentifier(table)
    },

    addLimitAndOffset: function(options, query) {

      query = query || ""

      if(options.order) {
          query += " OFFSET " + (options.offset || 0) + " ROWS";
          query += " FETCH NEXT " + (options.limit || 100000000) + " ROWS ONLY";
      }

      return query;
    }
  }

  var throwMethodUndefined = function(methodName) {
    throw new Error('The method "' + methodName + '" is not defined! Please add it to your sql dialect.');
  }

  return Utils._.extend(Utils._.clone(require("../abstract/query-generator")), QueryGenerator);
})()
