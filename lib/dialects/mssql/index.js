var _ = require('lodash')
    , Abstract = require('../abstract')

var MSSqlDialect = function(sequelize) {
    this.sequelize = sequelize
}

MSSqlDialect.prototype.supports = _.defaults({
    'RETURNING': false,
    'DEFAULT VALUES': false
}, Abstract.prototype.supports)

module.exports = MSSqlDialect

